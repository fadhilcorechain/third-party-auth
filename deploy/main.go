package main

import (
	"github.com/go-redis/redis"
	"fmt"
	"net/http"
	"math/rand"
	"time"
	"strconv"
	"io/ioutil"
	"github.com/buger/jsonparser"
	"encoding/json"
	"strings"
)

const PORT_NUMBER = 6033

type RoleObj struct{
	Pubkey string `json:"pubkey"`
	Roles []string `json:"roles"`
}

func conn() *redis.Client {
	client := redis.NewClient(&redis.Options{
		//Addr: "localhost:6379",
		//Addr:     "35.197.155.228:9967",
		Addr:     "35.194.1.0:6379",
		Password: "t3guhCakep", // no password set
		DB:       9,  // use default DB
	})
	return client
}

func main() {
	fmt.Println("==== OTP services ====")
	fmt.Println(fmt.Sprintf(" Run on Port :%d", PORT_NUMBER))
	fmt.Println("======================")
	rdb := conn()
	defer rdb.Close()
	mux := http.NewServeMux()
	mux.HandleFunc("/health", health)
	mux.HandleFunc("/apiv1/thirdparty/auth", auth(rdb))
	mux.HandleFunc("/apiv1/thirdparty/role/set", setRole(rdb))
	http.ListenAndServe(fmt.Sprintf(":%d", PORT_NUMBER), mux)
}

func health(w http.ResponseWriter, r *http.Request){
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(`{"message":"HEALTH"}`))
}

func auth(rdb *redis.Client) func(w http.ResponseWriter, r *http.Request){
	return func(w http.ResponseWriter, r *http.Request){
		code := http.StatusInternalServerError
		message := `{"message":"Service Unavailable"}`
		body,_ := ioutil.ReadAll(r.Body)
		pubKey,err := jsonparser.GetString(body,"pubkey")
		if err != nil{
			w.WriteHeader(code)
			return
		}

		unixTime := time.Now().Unix()
		strUnixTime := strconv.FormatInt(unixTime,10)
		apiKey := GenerateApiKey(strUnixTime)
		secretKey := GenerateSecretKey(strUnixTime)

		chk,_ := rdb.Exists(pubKey).Result()
		if(chk <= 0){
			rdb.HSet(pubKey,"apiKey",apiKey)
			rdb.HSet(pubKey,"secretKey",secretKey)
			code = http.StatusOK
			message = fmt.Sprintf(`{"apiKey":"%s","secretKey":"%s"}`,apiKey,secretKey)
		}
		w.WriteHeader(code)
		w.Write([]byte(message))
	}
}

func setRole(rdb *redis.Client) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		code := http.StatusInternalServerError
		message := `{"message":"Service Unavailable"}`
		body,_ := ioutil.ReadAll(r.Body)
		obj := RoleObj{}

		err := json.Unmarshal(body,&obj)
		if err != nil{
			w.WriteHeader(code)
			return
		}
		objMsg := ``
		resp := ``
		if len(obj.Pubkey) > 0{
			for _,v := range obj.Roles{
				objMsg = fmt.Sprintf(`%s,"%s"`,objMsg,v)
			}
			objMsg = strings.Replace(objMsg,",","",1)
			resp = fmt.Sprintf(`{"roles":[%s]}`,objMsg)

			rdb.HSet(obj.Pubkey,"role",resp)
			code = http.StatusOK
			message = resp
		}

		w.WriteHeader(code)
		w.Write([]byte(message))
	}
}

func GenerateApiKey(unixTime string) string {
	var letter = []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

	b := make([]rune, 10)
	for i := range b {
		b[i] = letter[rand.Intn(len(letter))]
	}
	return fmt.Sprintf("%s%s",unixTime,string(b))
}

func GenerateSecretKey(unixTime string) string {
	var letter = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+")

	b := make([]rune, 20)
	for i := range b {
		b[i] = letter[rand.Intn(len(letter))]
	}
	return fmt.Sprintf("%s%s",unixTime,string(b))
}